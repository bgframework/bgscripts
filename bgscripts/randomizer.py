# Import modules
import argparse
import logging
import os
import csv
import re

import numpy as np
from multiprocessing import Pool

import bgscripts.utils.loaddata as loaddata
import bgscripts.utils.referencegenome as refgen


# Global variables
LINE_PER_JOBS = 100000


class Randomizer(object):

    def __init__(self, total_jobs, input_file, regions, mutation_type, window_size, start_at_0):
        self.total_jobs = total_jobs
        self.input_file = input_file
        self.regions = regions
        self.mutation_type = mutation_type
        self.window_size = window_size
        self.start_at_0 = start_at_0

    def run(self, module):
        """Performs the randomization"""
        # Take into account if the mutations are 0 based or 1 based
        offset = 1 if self.start_at_0 is True else 2

        muts = []
        for mut in loaddata.load_mutations_by_module(self.input_file, module, self.total_jobs, self.regions):
            if (mut['TYPE'] == 'subs' and self.mutation_type == 'indel') or \
               (mut['TYPE'] == 'indel' and self.mutation_type == 'subs'):
                continue

            # Mutation signature
            if mut['TYPE'] == 'indel':
                signature = refgen.get_ref_duo(mut['CHROMOSOME'], mut['POSITION'],
                                               mut['REF'], mut['ALT'], offset).upper()
            elif mut['TYPE'] == 'subs':
                signature = refgen.get_ref_triplet(mut['CHROMOSOME'], mut['POSITION'], offset).upper()
            else:
                logging.warning("Unknown mutation type: {}".format(mut['TYPE']))
                continue

            # Sequence window
            window_start = int(mut['POSITION'] - (self.window_size / 2))
            window_start = 0 if window_start < 0 else window_start
            sequence = refgen.get_ref(mut['CHROMOSOME'], window_start, size=self.window_size)

            # Find all positions with the same signature
            positions = [window_start + m.start() + offset for m in re.finditer(signature, sequence)]
            positions = [p for p in positions if p != mut['POSITION']]

            # Remove positions outside our regions
            if self.regions is not None:
                positions = [p for p in positions if len(self.regions[mut['CHROMOSOME']][p]) > 0]

            if len(positions) == 0:
                logging.error("No positions with same signature at {}:{}".format(mut['CHROMOSOME'],
                                                                                 mut['POSITION']))
                continue

            if len(positions) < 10:
                logging.warning("Only {} positions with same signature at {}:{}".format(len(positions),
                                                                                        mut['CHROMOSOME'],
                                                                                        mut['POSITION']))

            # Get a random position
            random_position = np.random.choice(positions, 1)[0]

            mut['ORIGINAL_POSITION'] = mut['POSITION']
            mut['POSITION'] = random_position
            mut['SIGNATURE'] = signature
            muts.append(mut)

        return muts


def define_number_jobs(input_file, cores):
    total_lines = 0
    for _ in open(input_file, 'r'):
        total_lines += 1

    return max(cores, total_lines // LINE_PER_JOBS)


def randomize_dataset(input_file, output_file, mutation_type='all', regions=None,
                      start_at_0=False, cores=1, window_size=50000, seed=-1, quite=False):
    """Loads the regions and the mutations. Calls the randomization"""
    if output_file is None:
        output_file = "{}.rand".format(os.path.basename(input_file))

    # Set the seed
    if seed >= 0:
        set_seed(seed)

    # Load regions
    if regions is not None:
        logging.info("Loading regions...")
        regions_tree = loaddata.build_regions_tree(loaddata.load_regions(regions))
    else:
        regions_tree = None

    # Define the number of jobs
    total_jobs = define_number_jobs(input_file, cores)
    logging.info("Creating {} jobs".format(total_jobs))

    # Create randomizer
    randomizer = Randomizer(total_jobs, input_file, regions, mutation_type, window_size, start_at_0)

    logging.info("Randomizing mutations...")
    with Pool(processes=cores) as pool:
        with open(output_file, 'wt') as fd:
            writer = csv.DictWriter(fd, delimiter='\t', fieldnames=loaddata.MUTATIONS_HEADER)
            writer.writerow({fn: fn for fn in loaddata.MUTATIONS_HEADER})

            for muts in pool.imap(randomizer.run, range(total_jobs)):
                for mut in muts:
                    writer.writerow(mut)

    logging.info("Randomization [done]")


def set_seed(seed):
    """Set the numpy seed generator"""
    np.random.seed(seed)


def cmdline():
    """Command line parser"""
    # Parse the arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input",
                        dest="input_file",
                        required=True,
                        help="Input dataset to randomize")
    parser.add_argument("-o", "--output",
                        dest="output_file",
                        default=None,
                        help="Output randomized dataset")
    parser.add_argument("-t", "--type",
                        dest="mutation_type",
                        default='both',
                        choices=['subs', 'indel', 'all'],
                        help="Type of mutation: subs, indel, or all. By default it randomizes all")
    parser.add_argument('--0-based',
                        dest='start_at_0',
                        default=False,
                        action='store_true',
                        help="The positions of the mutations are 0 based. Default is False and the positions " +
                             "are considered as 1 based")
    parser.add_argument("-r", "--regions",
                        dest="regions",
                        default=None,
                        help="Randomize only mutations in this regions and keep the random positions inside")
    parser.add_argument("--window",
                        dest="window_size",
                        type=int,
                        default=50000,
                        help="Randomization total window size (default 50k), half at each side of the mutation.")
    parser.add_argument("--seed",
                        dest="seed",
                        type=int,
                        default=-1,
                        help="Set the seed generator for the randomization. By default it is chosen randomly")
    parser.add_argument("--cores",
                        dest="cores",
                        type=int,
                        default=os.cpu_count(),
                        choices=list(range(1, os.cpu_count() + 1)),
                        help="Maximum CPU cores to use (default all)")
    parser.add_argument('--quite',
                        dest='quite',
                        default=False,
                        action='store_true',
                        help="Hide any progress")
    args = parser.parse_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%H:%M:%S',
                        level=logging.INFO if not args.quite else logging.ERROR)

    randomize_dataset(input_file=args.input_file,
                      output_file=args.output_file,
                      mutation_type=args.mutation_type,
                      regions=args.regions,
                      start_at_0=args.start_at_0,
                      cores=args.cores,
                      window_size=args.window_size,
                      seed=args.seed,
                      quite=args.quite)

if __name__ == "__main__":
    cmdline()
