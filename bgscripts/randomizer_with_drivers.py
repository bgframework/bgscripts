"""Randomizes a dataset of mutations preserving the drivers mutations. Some regions are blacklisted:
- blacklisted by UCSC browser: /projects_bg/bg/users/sabari/noncoding/exon-intron/dataset/mappability/Duke_DAC_exclude_bed.gz
- low complexity: /projects_bg/bg/users/sabari/noncoding/exon-intron/dataset/mappability/hg19_low_complexity_regions.gz
- low mappability: /data/users/loris/pileup_mappability/hg19_100bp.coverage.gz
"""


# Import modules
import os
import re
import gzip
import csv
import configparser

import click
import colorlog
import bitarray
import numpy as np
from multiprocessing import Pool
from collections import defaultdict

from tqdm import tqdm
from intervaltree import IntervalTree

import bgscripts.utils.loaddata as loaddata
import bgscripts.utils.referencegenome as refgen


# Configure the colorlog module
logger = colorlog.getLogger()


def set_logger(level):
    global logger
    d = {'info': colorlog.colorlog.logging.INFO,
         'warning': colorlog.colorlog.logging.WARNING,
         'error': colorlog.colorlog.logging.ERROR}
    logger.setLevel(d[level])
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter())
    logger.addHandler(handler)


# Global variables
configs = configparser.ConfigParser()
configs.read(os.path.join(os.path.dirname(__file__), 'bgscripts.cfg'))
BLACKLISTED = configs['data']['blacklist']  # '/home/loris/projects/bgscripts/bgscripts/data/Duke_DAC_exclude_bed.gz'
LOW_MAPPABILITY = configs['data']['coverage']  # '/home/loris/projects/bgscripts/bgscripts/data/coverage/'
LOW_COMPLEXITY = configs['data']['low_complexity']  # '/home/loris/projects/bgscripts/bgscripts/data/hg19_low_complexity_regions.gz'
LINES_PER_JOBS = int(configs['multiprocess']['lines_per_job'])
SINGLETON = None


def run_randomizer_task(module):
    return SINGLETON.run(module)


def run_randomizer(instance, modules, processes=None):
    global SINGLETON
    SINGLETON = instance

    with Pool(processes=processes) as pool:
        for r in pool.imap_unordered(run_randomizer_task, modules):
            yield r


def define_number_jobs(input_file, cores):
    """Define how many lines of the mutation file each core has to compute.
    :param input_file: path, file with mutations
    :param cores: int, number of cores
    :return: int, lines that each core has to compute
    """
    total_lines = 0
    with open(input_file, 'r') as fd:
        for _ in fd:
            total_lines += 1
    return max(cores, total_lines // LINES_PER_JOBS)


def set_seed(seed):
    """Set the numpy seed generator
    :param seed: int, seed to use
    :return: None
    """
    np.random.seed(seed)


def read_mappability():
    """Read the mappability values of a chromosome
    :return: dict, dictionary of bitarray object 
    """
    coverage = {}
    for chromosome in [str(i) for i in range(1, 23)] + ['X', 'Y']:
        with open(os.path.join(LOW_MAPPABILITY, 'chr{}.bin'.format(chromosome)), "rb") as fd:
            values = bitarray.bitarray()
            values.fromfile(fd)
            coverage[chromosome] = values
    return coverage


COVERAGE = read_mappability()


class Randomizer:
    """Class to compute the randomization"""

    def __init__(self, total_jobs, input_file, regions, mutation_type, window_size, start_at_0, drivers,
                 only_in_regions, remove_unmappable, remove_blacklisted, remove_low_complexity):
        """Initialize the Randomizer class.
        :param total_jobs: int, total number of jobs
        :param input_file: path, file with mutations
        :param regions: path, file with regions where randomization should be done
        :param mutation_type: str, can be 'subs', 'indel', or 'all'
        :param window_size: int, window around each mutations that define the space where the simulated mutation can be
        :param start_at_0: boolean, if True the first position of a chromosome is 0, otherwise is 1
        :param drivers: set or None, drivers elements
        :param only_in_regions: boolean, if True mutations are simulated only within regions
        :param remove_unmappable: boolean, if True do not simulate a mutation in a not mappable region
        :param remove_blacklisted: boolean, if True do not simulate a mutation in a UCSC blacklisted region
        :param remove_low_complexity: boolean, if True do not simulate a mutation in a low complexity region
        """
        self.total_jobs = total_jobs
        self.input_file = input_file
        self.mutation_type = mutation_type
        self.window_size = window_size
        self.offset = 1 if start_at_0 is True else 2
        self.drivers = drivers
        self.only_in_regions = only_in_regions
        self.regions = self.build_regions(regions, 'genomic') if regions is not None else None
        self.remove_unmappable = remove_unmappable
        blacklisted = self.build_regions(BLACKLISTED, 'blacklisted') if remove_blacklisted else None
        low_complexity = self.build_regions(LOW_COMPLEXITY, 'low complexity') if remove_low_complexity else None
        self.excluded = self.merge_trees(blacklisted, low_complexity)

    @staticmethod
    def build_regions(regions_file, description):
        """Create a IntervalTree regions dictionary
        :param regions_file: path, path of the regions file
        :param description: str, description of the file
        :return: dict, IntervalTree regions dictionary
        """
        logger.info("Loading %s regions...", description)
        regions_tree = defaultdict(IntervalTree)
        with gzip.open(regions_file) as fd:
            for line in fd:
                line = line.decode().strip().split('\t')
                chromosome, start, end = line[:3]
                if len(line) > 3:
                    name = line[-1]
                else:
                    name = None
                if chromosome.startswith('chr'):
                    chromosome = chromosome[3:]
                regions_tree[chromosome][int(start): int(end) + 1] = name
        return regions_tree

    @staticmethod
    def merge_trees(tree_1, tree_2):
        """Merge two tree into a single one
        :param tree_1: dict or None, firsts trees of IntervalTree
        :param tree_2: dict or None, seconds trees of IntervalTree
        :return: dict of IntervalTree or None
        """
        if tree_1 is None and tree_2 is None:
            return None
        elif tree_1 is None:
            return tree_2
        elif tree_2 is None:
            return tree_1
        else:
            merged = dict()
            keys = set(tree_1.keys()) | set(tree_2.keys())
            for key in keys:
                merged[key] = tree_1[key] | tree_2[key]
                merged[key].merge_overlaps()
            return merged

    def is_driver_mutation(self, chromosome, position):
        """Check whether a mutations is inside a driver element
        :param chromosome: str, chromosome where the mutation is
        :param position: int, genomic position of the mutation
        :return: boolean, True if the mutation is driver False otherwise
        """
        return len(self.driver_mutation(chromosome, position)) > 0

    def driver_mutation(self, chromosome, position):
        """Check whether a mutations is inside a driver element
        :param chromosome: str, chromosome where the mutation is
        :param position: int, genomic position of the mutation
        :return: set, 
        """
        return set([i.data for i in self.regions[chromosome][position]]) & self.drivers

    def read_mappability(self, chromosome, start, end):
        """Read the mappability values of a chromosome
        :param chromosome: str, chromosome
        :param start: int, start position
        :param end: int, end position
        :return: bitarray object 
        """
        with open(os.path.join(LOW_MAPPABILITY, 'chr{}.bin'.format(chromosome)), "rb") as fd:
            values = bitarray.bitarray()
            values.fromfile(fd)
        return values[start: end + 1]

    def run(self, module):
        """Performs the randomization.
        :param module: int, job number
        :return: list, list of rows
        """
        muts = []
        observed_total = 0
        observed_excluded = 0
        observed_drivers = 0
        simulated_excluded = 0
        simulated_included = 0
        for mut in loaddata.load_mutations_by_module(
                input_file=self.input_file, module=module, size=self.total_jobs,
                regions_tree=self.regions, use_region=self.only_in_regions):
            if (mut['TYPE'] == 'subs' and self.mutation_type == 'indel') or \
               (mut['TYPE'] == 'indel' and self.mutation_type == 'subs'):
                continue
            # Mutation signature
            if mut['TYPE'] == 'indel':
                signature = refgen.get_ref_duo(
                    mut['CHROMOSOME'], mut['POSITION'], mut['REF'], mut['ALT'], self.offset
                ).upper()
            elif mut['TYPE'] == 'subs':
                signature = refgen.get_ref_triplet(
                    mut['CHROMOSOME'], mut['POSITION'], self.offset
                ).upper()
            else:
                logger.warning("Unknown mutation type: {}".format(mut['TYPE']))
                continue

            observed_total += 1

            # Check if the mutation is in a excluded region
            if self.excluded is not None and self.excluded[mut['CHROMOSOME']].overlaps_point(mut['POSITION']):
                logger.warning('Mutation in %s:%s has been excluded', mut['CHROMOSOME'], mut['POSITION'])
                observed_excluded += 1
                continue
            # Check if the mutation is in a not mappable region
            elif self.remove_unmappable and not COVERAGE[mut['CHROMOSOME']][mut['POSITION']]:
                logger.warning('Mutation in %s:%s is not mappable', mut['CHROMOSOME'], mut['POSITION'])
                observed_excluded += 1
                continue
            # Check if the mutation is inside a drivers region
            elif self.drivers is not None and len(self.drivers) > 0 and self.is_driver_mutation(mut['CHROMOSOME'], mut['POSITION']):
                random_position = mut['POSITION']
                driver = ','.join(self.driver_mutation(mut['CHROMOSOME'], mut['POSITION']))
                observed_drivers += 1
            else:
                # Sequence window
                window_start = int(mut['POSITION'] - (self.window_size / 2))
                window_start = 0 if window_start < 0 else window_start
                window_end = window_start + self.window_size

                # excluded = self.excluded and len(self.excluded[mut['CHROMOSOME']][window_start: window_end]) > 0
                excluded = set([mut['POSITION']])
                if self.excluded:
                    for tree in self.excluded[mut['CHROMOSOME']][window_start: window_end]:
                        excluded |= set(range(tree.begin, tree.end))

                if self.only_in_regions:
                    include = set()
                    for tree in self.regions[mut['CHROMOSOME']][window_start: window_end]:
                        include |= set(range(tree.begin, tree.end))

                sequence = refgen.get_ref(mut['CHROMOSOME'], window_start, size=self.window_size)

                # Find all positions with the same signature
                positions = {window_start + m.start() + self.offset for m in re.finditer(signature, sequence)}

                # Remove positions outside the genomic regions
                if self.only_in_regions:
                    # positions = [p for p in positions if self.regions[mut['CHROMOSOME']].overlaps_point(p)]
                    positions &= include

                # Remove positions in not mappable regions
                if self.remove_unmappable:
                    positions = set([p for p in positions if COVERAGE[mut['CHROMOSOME']][p]])

                # Remove positions in blacklisted and/or low_complexity regions
                if self.excluded:
                    # positions = [p for p in positions if not self.excluded[mut['CHROMOSOME']].overlaps_point(p)]
                    positions.difference_update(excluded)

                if len(positions) == 0:
                    logger.error("No positions with same signature at {}:{}".format(mut['CHROMOSOME'], mut['POSITION']))
                    simulated_excluded += 1
                    continue

                if len(positions) < 10:
                    logger.warning("Only {} positions with same signature at {}:{}".format(
                        len(positions), mut['CHROMOSOME'], mut['POSITION'])
                    )

                # Get a random position
                random_position = np.random.choice(list(positions), 1)[0]
                driver = ''
                simulated_included += 1

            mut['ORIGINAL_POSITION'] = mut['POSITION']
            mut['POSITION'] = random_position
            mut['SIGNATURE'] = signature
            if self.drivers is not None:
                mut['DRIVER'] = driver
            muts.append(mut)
        return muts, observed_total, observed_excluded, observed_drivers, simulated_excluded, simulated_included


def read_drivers_list(input_drivers):
    """Read a file of driver elements
    :param input_drivers: path, file with the drivers list. The file is expected to have the name
    of the drivers in the first column
    :return: set, list of drivers or None
    """
    if input_drivers is None:
        return None
    drivers = set()
    with open(input_drivers, 'r') as fd:
        for line in fd:
            drivers.add(line.strip().split('\t')[0])
    return drivers


def randomize_dataset(input_file, output_file, input_drivers, regions, only_in_regions,
                      remove_unmappable, remove_blacklisted, remove_low_complexity,
                      start_at_0, window_size, mutation_type, cores, seed):
    """Loads the regions and the mutations. Calls the randomization
    :param input_file: path, file with mutations
    :param output_file: path, file where to save the simulated dataset
    :param input_drivers: path or None, file with drivers coordinates to spike in
    :param regions: path, file with regions where randomization should be done
    :param only_in_regions: boolean, if True mutations are simulated only within regions
    :param remove_unmappable: boolean, if True do not simulate a mutation in a not mappable region
    :param remove_blacklisted: boolean, if True do not simulate a mutation in a UCSC blacklisted region
    :param remove_low_complexity: boolean, if True do not simulate a mutation in a low complexity region
    :param start_at_0: boolean, if True the first position of a chromosome is 0, otherwise is 1
    :param window_size: int, window around each mutations that define the space where the simulated mutation can be
    :param mutation_type: str, can be 'subs', 'indel', or 'all'
    :param cores: int, number of cpu to use
    :param seed: int, seed of the random generator
    :return: None
    """
    if output_file is None:
        output_file = "{}.simulation".format(os.path.basename(input_file))

    # Set the seed
    if seed >= 0:
        logger.info("Set seed to {}".format(seed))
        set_seed(seed)

    # Load the driver genes list
    drivers = read_drivers_list(input_drivers)

    # Define the total number of jobs
    total_jobs = define_number_jobs(input_file, cores)
    logger.info("Creating {} job{}".format(total_jobs, 's' if cores > 1 else ''))

    # Create an instance of the Randomizer class
    randomizer = Randomizer(total_jobs, input_file, regions, mutation_type, window_size, start_at_0,
                            drivers, only_in_regions, remove_unmappable, remove_blacklisted, remove_low_complexity)

    logger.info("Randomization [start]")

    # Read the header of the mutation file
    with open(input_file, 'r') as fd:
        header = fd.readline().strip().split('\t') + ["ORIGINAL_POSITION", "SIGNATURE"]
        if input_drivers is not None:
            header += ["DRIVER"]

    observed_total = 0
    observed_excluded = 0
    observed_drivers = 0
    simulated_excluded = 0
    simulated_included = 0
    simulated_drivers = set([])

    with tqdm(total=total_jobs) as pbar:
        with open(output_file, 'wt') as fd:
            writer = csv.DictWriter(fd, delimiter='\t', fieldnames=header, extrasaction="ignore")
            writer.writerow({fn: fn for fn in header})
            for (muts, obs_total, obs_excluded, obs_drivers,
                 sim_excluded, sim_included) in run_randomizer(randomizer, range(total_jobs), processes=cores):
                observed_total += obs_total
                observed_excluded += obs_excluded
                observed_drivers += obs_drivers
                simulated_excluded += sim_excluded
                simulated_included += sim_included
                pbar.update(1)
                for mut in muts:
                    writer.writerow(mut)
                    if input_drivers is not None and mut['DRIVER'] != '':
                        simulated_drivers.add(mut['DRIVER'])
    logger.info("Randomization [done]")
    logger.info('Mutations analyzed: %s', observed_total)
    logger.info('Mutations excluded: %s (%0.1f%s)', observed_excluded, observed_excluded / observed_total * 100, '%')
    if input_drivers is not None:
        logger.info('Mutations drivers: %s (%0.1f%s)', observed_drivers, observed_drivers / observed_total * 100, '%')
        logger.info('Simulated drivers: %s (%s)',
                    len(simulated_drivers), ','.join(simulated_drivers) if len(simulated_drivers) > 0 else '-')
    simulated_total = simulated_included + simulated_excluded
    logger.info('Mutations simulated: %s out of %s', simulated_included, simulated_total)
    logger.info('Mutations simulated excluded: %s (%0.1f%s)',
                simulated_excluded, simulated_excluded / simulated_total * 100, '%')


@click.command()
@click.argument('mutations_file', type=click.Path(exists=True))  # , help='file with the mutations to simulate')
@click.argument('output_file')  # , help='path of the file to create with the simulated results')
@click.option('-d', '--drivers-list', default=None, type=click.Path(exists=True),
              help='list of drivers to spike in')
@click.option('-m', '--mutation-type', default='all', type=click.Choice(['subs', 'indel', 'all']),
              help='type of mutation to simulate: all, subs, or indel. Default is all.')
@click.option('-r', '--regions', default=None, type=click.Path(exists=True),
              help='file with the coordinates of genomic regions. ' +
                   'To be used with --only-in-regions and/or --drivers-list')
@click.option('--only-in-regions', is_flag=True,
              help='Mutations are randomized only within regions.')
@click.option('--remove-unmappable', is_flag=True,
              help='do not simulate mutations in not mappable regions')
@click.option('--remove-blacklisted', is_flag=True,
              help='do not simulate mutations in UCSC blacklisted regions')
@click.option('--remove-low-complexity', is_flag=True,
              help='do not simulate mutations in low-complexity regions')
@click.option('--start-at-0', is_flag=True,
              help='if True the first position of a chromosome is 0, otherwise is 1')
@click.option('-c', '--cores', type=click.IntRange(min=1, max=os.cpu_count(), clamp=False),
              default=os.cpu_count(),
              help='Number of cores to use in the computation. By default it uses all the available cores.')
@click.option('-w', '--window-size', type=click.INT, default=50000,
              help='window around each mutations that define the space where the simulated mutation can be. ' +
                   'By default uses a window of 50000 bp.')
@click.option('-s', '--seed', type=click.INT, default=-1, help='seed of the random generator')
@click.option('--log-level', default='info', type=click.Choice(['info', 'warning', 'error']),
              help='verbosity of the logger. By default is info.')
def main(mutations_file, output_file, drivers_list, mutation_type, start_at_0, cores, window_size, seed, log_level,
         only_in_regions, remove_unmappable, remove_blacklisted, remove_low_complexity, regions=None):
    """Simulate the mutations of a dataset"""
    if regions is None and (only_in_regions or drivers_list):
        raise click.UsageError('Missing the path of the regions coordinates.')
    set_logger(log_level)
    randomize_dataset(
        input_file=mutations_file,
        output_file=output_file,
        input_drivers=drivers_list,
        mutation_type=mutation_type,
        regions=regions,
        only_in_regions=only_in_regions,
        remove_unmappable=remove_unmappable,
        remove_blacklisted=remove_blacklisted,
        remove_low_complexity=remove_low_complexity,
        start_at_0=start_at_0,
        cores=int(cores),
        window_size=window_size,
        seed=seed,
    )


if __name__ == '__main__':
    main()
