import os
import mmap
import bgdata

HG19 = bgdata.get_path('datasets', 'genomereference', 'hg19')
HG19_MM = {}
COMPLEMENTS = dict(zip('ACGTNacgtn', 'TGCANtgcan'))
CODONS = {
    'TTT': 'F', 'TTC': 'F', 'TTA': 'L', 'TTG': 'L', 'TCT': 'S',
    'TCC': 'S', 'TCA': 'S', 'TCG': 'S', 'TAT': 'Y', 'TAC': 'Y',
    'TGT': 'C', 'TGC': 'C', 'TGG': 'W', 'CTT': 'L', 'CTC': 'L',
    'CTA': 'L', 'CTG': 'L', 'CCT': 'P', 'CCC': 'P', 'CCA': 'P',
    'CCG': 'P', 'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R', 'ATT': 'I',
    'ATC': 'I', 'ATA': 'I', 'ATG': 'M', 'ACT': 'T', 'ACC': 'T',
    'ACA': 'T', 'ACG': 'T', 'AAT': 'N', 'AAC': 'N', 'AAA': 'K',
    'AAG': 'K', 'AGT': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
    'GTT': 'V', 'GTC': 'V', 'GTA': 'V', 'GTG': 'V', 'GCT': 'A',
    'GCC': 'A', 'GCA': 'A', 'GCG': 'A', 'GAT': 'D', 'GAC': 'D',
    'GAA': 'E', 'GAG': 'E', 'GGT': 'G', 'GGC': 'G', 'GGA': 'G',
    'GGG': 'G', }
STOP_CODONS = ('TAA', 'TAG', 'TGA')
START_CODONS = ('TTG', 'CTG', 'ATG')


def get_mmap(chromosome):
    if chromosome not in HG19_MM:
        fd = open(os.path.join(HG19, "chr{0}.txt".format(chromosome)), 'rb')
        HG19_MM[chromosome] = mmap.mmap(fd.fileno(), 0, access=mmap.ACCESS_READ)
    return HG19_MM[chromosome]


def get_ref(chromosome, start, size=1):
    mm_fd = get_mmap(chromosome)
    mm_fd.seek(start)
    return mm_fd.read(size).decode().upper()


def reverse_complement(sequence):
    """Return the reverse complement of a genomic sequence"""
    seq = list(sequence)
    seq.reverse()
    return ''.join([COMPLEMENTS.get(i, i) for i in seq])


def get_ref_triplet(chromosome, start, offset):
    """Get a trinucleotide from the reference genome. The parameter offset adjust the position by
    considering whether the coordinates are 0-based (offset of 1) or 1-based (offset of 2).
    """
    return get_ref(chromosome, start - offset, size=3)


def get_ref_duo(chromosome, start, ref, alt, offset):
    """Get a dinucleotide from the reference genome. If the mutations is a deletion
    the returned dinucleotide will include as many '.' as the length of the deletion.
    The parameter offset adjust the position by considering whether the coordinates
    are 0-based (offset of 1) or 1-based (offset of 2).
    """
    # Insertion
    if ref == '-':
        return get_ref(chromosome, start - offset, size=2)
    # Deletion
    else:
        length = len(ref)
        deletion = get_ref(chromosome, start - offset, size=length + 2)
        return deletion[0] + "." * length + deletion[-1]

