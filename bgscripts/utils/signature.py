# Import modules
import pickle
import csv
from collections import defaultdict

import colorlog
import click
from tqdm import tqdm

import bgscripts.utils.referencegenome as refgen


# Configure the colorlog module
logger = colorlog.getLogger()


def set_logger(level):
    global logger
    d = {'info': colorlog.colorlog.logging.INFO,
         'warning': colorlog.colorlog.logging.WARNING,
         'error': colorlog.colorlog.logging.ERROR}
    logger.setLevel(d[level])
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter())
    logger.addHandler(handler)


class Parser:
    """Class to parse the datasets with somatic mutations"""

    def __init__(self):
        self.CHROMOSOME = 'CHROMOSOME'
        self.POSITION = 'POSITION'
        self.REF = 'REF'
        self.ALT = 'ALT'
        self.SAMPLE = 'SAMPLE'
        self.TYPE = 'TYPE'
        self.CANCER_TYPE = 'CANCER_TYPE'
        self.SIGNATURE = 'SIGNATURE'
        self.TRANSCRIPT = 'TRANSCRIPT'
        self.SYMBOL = 'SYMBOL'


class Signature:
    """Class to calculate the mutational signatures of a dataset"""

    def __init__(self, start_at_0=False, mutation_type='subs'):
        self.mutation_type = mutation_type
        self.start_at_0 = start_at_0
        self.signatures = {'counts': defaultdict(int), 'probabilities': defaultdict(int)}

    def save(self, signature_file):
        """Save the signature to an output file"""
        with open(signature_file, 'wb') as fd:
            pickle.dump(self.signatures, fd)

    @staticmethod
    def load(signature_file):
        """Load precalculated signatures"""
        with open(signature_file, 'rb') as fd:
            signatures = pickle.load(fd)
        return signatures

    def calculate(self, mutations_file):
        """Calculate the signature of a dataset"""
        parser = Parser()

        # Take into account if the mutations are 0 based or 1 based
        offset = 1 if self.start_at_0 is True else 2

        with open(mutations_file, 'r') as csvfile:
            fd = csv.DictReader(csvfile, delimiter='\t')
            count = 0
            for line in tqdm(fd):
                chromosome = line[parser.CHROMOSOME]
                position = int(line[parser.POSITION])
                ref = line[parser.REF]
                alt = line[parser.ALT]
                mutation_type = line[parser.TYPE]
                if mutation_type != 'subs':
                    continue
                signature_ref = refgen.get_ref_triplet(chromosome, position, offset).upper()
                signature_alt = ''.join([ref[0], alt, ref[-1]])
                self.signatures['counts'][(signature_ref, signature_alt)] += 1
                count += 1
        self.signatures['probabilities'] = {k: v / count for k, v in self.signatures['counts'].items()}


@click.command()
@click.argument('input_file')
@click.argument('output_file')
@click.option('--log-level', default='info', help='verbosity of the logger',
              type=click.Choice(['info', 'warning', 'error']))
@click.option('--start_at_0', is_flag=True)
def main(input_file, output_file, start_at_0, log_level):
    """Calculate the signature of a dataset"""
    set_logger(log_level)
    signature = Signature(start_at_0=start_at_0)
    signature.calculate(input_file)
    signature.save(output_file)


if __name__ == '__main__':
    main()
