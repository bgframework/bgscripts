"""Function to test the outcomes of randomizer.py
The tests work by compare the output of the script with 3 expected outputs,
one for each mutation type (subs, indel, all). In order to compare the
outputs it is important to set the seed to 42.
"""

# Import modules to test
from bgscripts import randomizer
import functools

# Global variables
input_file = 'tests/test_dataset.input'
output_file = 'tests/outputs/test_dataset.output'
control_file = 'tests/outputs/test_dataset.expected_{}'


# Partial function to run the randomization
run_randomization = functools.partial(randomizer.randomize_dataset,
                                      input_file=input_file,
                                      output_file=output_file,
                                      regions=None,
                                      start_at_0=False,
                                      cores=1,
                                      window_size=50000,
                                      seed=42,
                                      quite=False)


def is_equal(file_1, file_2):
    """Assert two file are equivalent"""
    set_1 = set([line for line in open(file_1, 'r')])
    set_2 = set([line for line in open(file_2, 'r')])
    return set_1 == set_2


def test_randomize_subs():
    """Test the basic functionality of randomizer.py"""
    run_randomization(mutation_type='subs')
    assert is_equal(output_file, control_file.format('subs')) is True


def test_randomize_indel():
    """Test the basic functionality of randomizer.py"""
    run_randomization(mutation_type='indel')
    assert is_equal(output_file, control_file.format('indel')) is True


def test_randomize_all():
    """Test the basic functionality of randomizer.py"""
    run_randomization(mutation_type='all')
    assert is_equal(output_file, control_file.format('all')) is True
